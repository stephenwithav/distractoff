function loadTemplate(path, domElem)
{
	var xhr = new XMLHttpRequest();
	xhr.open('GET', chrome.extension.getURL(path), true);
	xhr.onreadystatechange = function()
	{
		if(xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200)
			domElem.innerHTML = xhr.responseText;
	};
	xhr.send(null);
}

document.addEventListener('DOMContentLoaded', function()
{
	var btnClose = document.getElementById('btn-close');

	var version =  document.getElementById('version');
	var changelog =  document.getElementById('changelog');

	version.textContent = chrome.runtime.getManifest().version;
	loadTemplate("template/changelog.html", changelog);

	btnClose.addEventListener("click", function()
	{
		window.close();
	});
});
