var helper = 
{
	extractDomain: function(url) 
	{
		var parser = document.createElement('a');
		parser.href = url;
	
		return parser.hostname;
	},
	
	extractElement: function(list, predicate)
	{
		for (var i = 0; i < list.length; i++)
		{
			if(predicate(list[i]))
				return list[i];
		}
	
		return null;
	}
};

var url = window.location.href;
var domain = helper.extractDomain(url);

function warn(callback)
{
	var fontLink  = document.createElement("link");
	fontLink.href = "https://fonts.googleapis.com/css?family=Roboto";
	fontLink.rel = "stylesheet";
	
	document.documentElement.appendChild(fontLink);
	
	var div  = document.createElement("div");
	div.className= "distract-off-warning-container";

	var xhr = new XMLHttpRequest();
	xhr.open('GET', chrome.extension.getURL('../template/warning.html'), true);
	xhr.onreadystatechange = function()
	{
		if(xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200)
		{
			div.innerHTML = xhr.responseText;
			document.documentElement.appendChild(div);

			document.head.innerHTML = "";
			document.body.className = "";
			document.body.innerHTML = "";

			callback();
		}
	};
	xhr.send();
}

function checkSchedule(sTime, sWeek)
{
	var now = new Date();
	var dayIndex = (now.getDay() + 6) % 7;

	var fromTime = new Date();
	var toTime = new Date();

	fromTime.setTime(now.getTime());
	fromTime.setHours(sTime[0][0]);
	fromTime.setMinutes(sTime[0][1]);
	fromTime.setSeconds(0);
	fromTime.setMilliseconds(0);

	toTime.setTime(now.getTime());
	toTime.setHours(sTime[1][0]);
	toTime.setMinutes(sTime[1][1]);
	toTime.setSeconds(0);
	toTime.setMilliseconds(0);

	var inTime = false;

	if(fromTime.getTime() === toTime.getTime())
		inTime = true;
	else if(fromTime <= toTime)
		inTime = now >= fromTime && now <= toTime;
	else
		inTime = now >= fromTime || now <= toTime;

	return inTime && sWeek[dayIndex];
}

chrome.storage.local.get({ bannedDomains: [], status:"Running", scheduleTime:[[0, 0], [0, 0]], 
	scheduleWeek:[true, true, true, true, true, true, true] }, function(result)
{
	var status = result.status;
	var scheduleTime = result.scheduleTime;
	var scheduleWeek = result.scheduleWeek;
	var bannedDomains = result.bannedDomains;
	var banned = helper.extractElement(bannedDomains, x => x === domain);

	if(banned && status === "Running" && checkSchedule(scheduleTime, scheduleWeek))
	{
		chrome.storage.local.get({ tempAllowed: [] }, function(result)
		{
			var tempAllowed = result.tempAllowed;
			var allowed = helper.extractElement(tempAllowed, x => x === domain);

			if(allowed === null)
			{
				warn(function()
				{

					var btnYes = document.getElementById("distract-off-btn-yes");
					var btnNo = document.getElementById("distract-off-btn-no");
					var btnSettings = document.getElementById("distract-off-btn-settings");
	
					btnSettings.addEventListener("click", function() { chrome.runtime.sendMessage({type:"settings"}); });
					btnYes.addEventListener("click", function() { chrome.runtime.sendMessage({type:"close"}); });
					btnNo.addEventListener("click", function()
					{
						var domain = helper.extractDomain(url);
						tempAllowed.push(domain);
					
						chrome.storage.local.set({ tempAllowed: tempAllowed }, function()
						{
							if(chrome.runtime.lastError)
							{
								console.log("Error: could not temp allow domain.");
								console.log(chrome.runtime.lastError);
								return;
							}
					
							location.reload();
						});
					});
				});
			}
		});
	}
});